#include <iostream>

using namespace std;

int s, pilihadmin,kesempatan_login = 2;
string username,password;
string username_admin = "BangJay";
string password_admin = "bangjaybuncit";
string username_kasir = "kasir";
string password_kasir = "123";

const int full = 10;
string barang[full]={
    "Tisu","Kopi","Roti","Shampo","Sabun","Pasta Gigi"
};

float harga[full]={
    7000,3000,9000,8000,5000,7000
};

int menuAwal = 6;

int pilih, jumlah, lagi;
char kasir_lagi;

void tampilBarang(){
    for(int i = 0 ; i< menuAwal; i++){
        cout << i+1 << ". " << barang[i] << " Rp." << harga [i] << endl; 
    }
}

void tambahBarang(){
    if(menuAwal < full){
        string barangBaru;
        float hargaBaru;

        cout << "Masukkan nama barang : ";
        cin.ignore();
        getline(cin, barangBaru);
        cout << "Masukkan harga barang : ";
        cin >> hargaBaru;

        barang[menuAwal] = barangBaru;
        harga[menuAwal] = hargaBaru;

        menuAwal++;
        cout << "Data berhasil ditambahkan" << endl;
    } else {
        cout << "Barang sudah maks" << endl;
    }
}

void ubahBarang() {
    int y;
    cout << "Pilih barang : ";
    cin >> y;

    cout << "Harga baru : ";
    cin >> harga[y-1];
}

void hapusBarang(){
    int jay;
    tampilBarang();
    cout << "Pilih barang yang ingin di hapus : ";
    cin >> jay;

    if(jay >= 1 && jay <= menuAwal){
        for(int i = jay -1; i <menuAwal - 1; i++) {
            barang[i]=barang [i+1];
            harga [i]=harga [i+1];
        }
        menuAwal-- ;
    } else{
        cout << "Barang tidak di temukan" << endl;
    }
}

void tampilanAdmin(){
    do{
        cout << "1. Lihat Data" << endl;
        cout << "2. Tambah Data" << endl;
        cout << "3. Ubah Data " << endl; 
        cout << "4. Hapus Data" << endl;
        cout << "5. Keluar" << endl;
        cout << "Masukan Pilihan : ";
        cin >> pilihadmin;

        switch(pilihadmin){
            case 1:
            char lagi;
            tampilBarang();
            cout << "Ketik 1 huruf apapun untuk kembali : ";
            cin >> lagi;
            system("cls");
            break;

            case 2:
            tampilBarang();
            tambahBarang();
            system("cls");
            break;

            case 3:
            tampilBarang();
            ubahBarang();
            system("cls");
            break;

            case 4:
            hapusBarang();
            system("cls");

        }
    }while(pilihadmin != 5);
}

void tampilanKasir() {
    const int maks_barang = 10;
    string barangDipilih[maks_barang];
    int jumlah_barang[maks_barang];
    float totalHarga = 0;
    int barang_dipilih = 0;

    do {
        tampilBarang();
        cout << "Pilih barang : ";
        cin >> pilih;

        if (pilih >= 1 && pilih <= menuAwal) {
            cout << "Jumlah : ";
            cin >> jumlah;

            totalHarga += harga[pilih - 1] * jumlah;

            barangDipilih[barang_dipilih] = barang[pilih - 1];
            jumlah_barang[barang_dipilih] = jumlah;
            barang_dipilih++;

            cout << "Lagi Y/T : ";
            cin >> kasir_lagi;

            system("cls"); // Untuk membersihkan layar setelah input
        } else {
            cout << "Barang tidak valid, silakan pilih lagi." << endl;
        }

    } while (kasir_lagi == 'y' || kasir_lagi == 'Y');

    // Menampilkan invoice
    cout << "===================================" << endl;
    cout << "               INVOICE              " << endl;
    cout << "===================================" << endl;
    cout << "No   Barang            Jumlah  Harga" << endl;
    cout << "-----------------------------------" << endl;

    for (int i = 0; i < barang_dipilih; i++) {
        const float hargaPerItem = harga[i];

        cout << i + 1 << ".   " << barangDipilih[i] << "           " << jumlah_barang[i] << "      Rp. " << hargaPerItem * jumlah_barang[i] << endl;
    }

    cout << "-----------------------------------" << endl;
    cout << "              Total Harga : Rp. " << totalHarga << endl;
    cout << "===================================" << endl;

    cout << "1. Kembali ke Menu Utama" << endl;
    cout << "2. Input Pelanggan Lagi" << endl;
    cout << "Pilih opsi: ";
    
    int opsi;
    cin >> opsi;

    if (opsi == 1) {
        // Kembali ke menu utama (tidak melakukan apa-apa, karena loop utama akan melanjutkan)
    } else if (opsi == 2) {
        system("cls");  // Membersihkan layar
        tampilanKasir();  // Rekursi untuk mengulang proses input pelanggan
    } else {
        cout << "Opsi tidak valid. Kembali ke menu utama." << endl;
    }
}




int main (){

do {
    
    cout << "Masukan username : ";
    cin >> username;
    cout << "Masukan Password : ";
    cin >> password;

    if(username==username_admin && password==password_admin){

        tampilanAdmin();
    }else if(username == username_kasir && password == password_kasir) {
        tampilanKasir();
    }
    
     else{
        cout<< "password salah, coba lagi" << endl;
        cout << "kesempatan anda " << kesempatan_login << endl;
    }
    kesempatan_login--;

}while(kesempatan_login >= 0);

    if(kesempatan_login <= 0){
        cout << "Anda Terblokir" << endl;
    }

    return 0 ;
}
